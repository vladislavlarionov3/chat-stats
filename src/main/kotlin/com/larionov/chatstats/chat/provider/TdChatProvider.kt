package com.larionov.chatstats.chat.provider

import com.larionov.chatstats.chat.Chat
import com.larionov.chatstats.td.ClientWrapper
import org.drinkless.tdlib.TdApi
import org.drinkless.tdlib.TdApi.Message
import org.drinkless.tdlib.TdApi.Messages
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.LocalDateTime.ofEpochSecond
import java.time.ZoneOffset.UTC
import java.util.concurrent.Callable
import java.util.concurrent.Executors

@Component
class TdChatProvider(private val client: ClientWrapper) : ChatProvider {
    private val executor = Executors.newFixedThreadPool(4)

    override fun getAll(ids: List<Long>, fromDate: LocalDate) = ids
        .map { executor.submit(Callable { getAll(it, fromDate) }).get()!! }

    fun getAll(id: Long, fromDate: LocalDate): Chat {
        val totalMessages = mutableListOf<Message>()

        var lastReceivedMessages: List<Message> = listOf()
        do {
            val lastReceivedMessageId = lastReceivedMessages.lastOrNull()?.id ?: 0
            lastReceivedMessages = TdApi.GetChatHistory(id, lastReceivedMessageId, 0, 100, false)
                .let { client.sendSync(it) as Messages }
                .messages.toList()
                .filter { fromDate <= ofEpochSecond(it!!.date.toLong(), 0, UTC).toLocalDate() }
            totalMessages.addAll(lastReceivedMessages)
        } while (lastReceivedMessages.isNotEmpty())

        return Chat(
            id.toString(),
            "",
            id,
            totalMessages.map {
                com.larionov.chatstats.chat.Message(it.id, "", ofEpochSecond(it.date.toLong(), 0, UTC))
            }
                .sortedBy { it.dateTime }
        )
    }
}