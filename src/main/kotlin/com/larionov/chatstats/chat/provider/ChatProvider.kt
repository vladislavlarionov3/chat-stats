package com.larionov.chatstats.chat.provider

import com.larionov.chatstats.chat.Chat
import java.time.LocalDate

interface ChatProvider {
    fun getAll(ids: List<Long>, fromDate: LocalDate): List<Chat>
}