package com.larionov.chatstats.chat.provider

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.larionov.chatstats.chat.Chat
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime

//@Component
class ExportedFileChatProvider : ChatProvider {
    private val chatFilePaths = listOf<String>()

    private val gson = GsonBuilder()
        .registerTypeAdapter(
            LocalDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                LocalDateTime.parse(json.asString)
            }
        )
        .create()

    fun load(filePath: String) = File(filePath).reader()
        .let { gson.fromJson(it, Chat::class.java)!! }

    override fun getAll(ids: List<Long>, fromDate: LocalDate): List<Chat> {
        TODO("add filter")
        return chatFilePaths.map(::load)
    }
}