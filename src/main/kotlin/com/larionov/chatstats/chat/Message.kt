package com.larionov.chatstats.chat

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

class Message(
    val id: Long,
    val type: String,
    @SerializedName("date") val dateTime: LocalDateTime,
//        val from: String,
//        @SerializedName("from_id") val fromId: String,
) {
    fun date() = dateTime.toLocalDate()!!
}