package com.larionov.chatstats.chat

import java.time.Duration

data class Chat(
    val name: String,
    val type: String,
    val id: Long,
    private val messages: List<Message>,
) {
    fun messages() = messages.mapIndexed { index, message ->
        ChatMessage(message, index)
    }

    fun dates() = messages.map { it.date() }.distinct()

    inner class ChatMessage(val message: Message, private val index: Int) {
        fun isFirst() = index == 0
        fun isLast() = index == messages.lastIndex

        fun next() = (index + 1).let { ChatMessage(messages[it], it) }
        private fun previous() = (index - 1).let { ChatMessage(messages[it], it) }

        fun betweenPreviousDuration() = Duration.between(previous().message.dateTime, message.dateTime)!!
    }

}