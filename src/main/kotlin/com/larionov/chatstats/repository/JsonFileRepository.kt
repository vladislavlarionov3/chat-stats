package com.larionov.chatstats.repository

import com.google.gson.*
import java.io.File
import java.lang.reflect.Type
import java.time.LocalDateTime
import java.time.ZoneOffset

class JsonFileRepository<E>(
    private val filePath: String,
    private val listType: Type
) {
    private val zoneOffset = ZoneOffset.UTC

    private val gson = GsonBuilder()
        .registerTypeAdapter(
            LocalDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                LocalDateTime.ofEpochSecond(json.asLong, 0, zoneOffset)
            }
        )
        .registerTypeAdapter(
            LocalDateTime::class.java,
            JsonSerializer { it: LocalDateTime, _, _ ->
                JsonPrimitive(it.toEpochSecond(zoneOffset))
            }
        )
        .create()


    fun save(all: List<E>) {
        File(filePath).writer()
            .use { gson.toJson(all, it) }
    }

    fun findAll(): List<E> = File(filePath).reader()
        .use { Gson().fromJson(it, listType) }

}