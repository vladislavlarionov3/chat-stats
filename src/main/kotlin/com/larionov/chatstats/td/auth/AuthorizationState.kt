package com.larionov.chatstats.td.auth

import org.drinkless.tdlib.TdApi
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

@Component
class AuthorizationState {
    val tdInternalState = AtomicReference<TdApi.AuthorizationState>()
    val code = AtomicReference<String>(null)
    val password = AtomicReference<String>(null)
    val authorized = AtomicBoolean()
}