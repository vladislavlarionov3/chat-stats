package com.larionov.chatstats.td.auth

import com.larionov.chatstats.td.ClientWrapper
import com.larionov.chatstats.td.TdProperties
import org.drinkless.tdlib.TdApi
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

@Component
class UpdateAuthorizationStateHandler(
    private val properties: TdProperties,
    private val state: AuthorizationState,
    @Lazy
    private val client: ClientWrapper
) {

    val dispatcher = Executors.newSingleThreadExecutor()

    fun handle(notification: TdApi.UpdateAuthorizationState?) {
        dispatcher.submit { state.handle(notification) }
    }

    private fun AuthorizationState.handle(notification: TdApi.UpdateAuthorizationState?) {
        TimeUnit.SECONDS.sleep(5)

        if (notification != null) {
            val newAuthorizationState = notification.authorizationState
            if (newAuthorizationState != null) {
                tdInternalState.set(newAuthorizationState)
            }
        }

        when (tdInternalState.get().constructor) {
            TdApi.AuthorizationStateWaitTdlibParameters.CONSTRUCTOR -> {
                client.send(getSetTdlibParameters(), ::logResult)
//                addProxy()
            }

            TdApi.AuthorizationStateWaitPhoneNumber.CONSTRUCTOR -> {
                client.send(TdApi.SetAuthenticationPhoneNumber(properties.phone, null), ::logResult)
            }

            TdApi.AuthorizationStateWaitOtherDeviceConfirmation.CONSTRUCTOR -> {
                val link = (tdInternalState.get() as TdApi.AuthorizationStateWaitOtherDeviceConfirmation?)?.link
                println("Please confirm this login link on another device: $link")
            }

            TdApi.AuthorizationStateWaitCode.CONSTRUCTOR -> {
                try {
                    println(notification)
                    code.waitForInput("Please enter authentication code")
                    client.send(TdApi.CheckAuthenticationCode(code.get()), ::logResult)
                } finally {
                    code.set(null)
                }
            }

            TdApi.AuthorizationStateWaitPassword.CONSTRUCTOR -> {
                try {
                    password.waitForInput("Please enter password")
                    client.send(TdApi.CheckAuthenticationPassword(password.get()), ::logResult)
                } finally {
                    password.set(null)
                }
            }

            TdApi.AuthorizationStateReady.CONSTRUCTOR -> {
                authorized.set(true)
                println("Authorization Ready")
            }

            TdApi.AuthorizationStateLoggingOut.CONSTRUCTOR -> {
                authorized.set(false)
                println("Authorization LoggingOut")
            }

            TdApi.AuthorizationStateClosing.CONSTRUCTOR -> {
                authorized.set(false)
                println("Authorization Closing")
            }

            TdApi.AuthorizationStateClosed.CONSTRUCTOR -> println("Authorization Closed")
            else -> println("ERROR: Unsupported authorization state:${state}")
        }
    }

    private fun <V> AtomicReference<V>.waitForInput(logMessage: String) {
        while (get() == null) {
            println(logMessage)
            TimeUnit.SECONDS.sleep(3)
        }
    }

    private fun logResult(result: TdApi.Object) {
        when (result.constructor) {
            TdApi.Ok.CONSTRUCTOR -> println("Ok")
            TdApi.Error.CONSTRUCTOR -> {
                println("ERROR: Receive an error:\n$result")
                handle(null) // repeat last action
            }

            else -> println("ERROR: Receive wrong response from TDLib:\n$result")
        }
    }

    private fun getSetTdlibParameters() = properties.run {
        TdApi.SetTdlibParameters(
            useTestDc,
            databaseDirectory,
            filesDirectory,
            databaseEncryptionKey.toByteArray(),
            useFileDatabase,
            useChatInfoDatabase,
            useMessageDatabase,
            useSecretChats,
            apiId,
            apiHash,
            systemLanguageCode,
            deviceModel,
            systemVersion,
            applicationVersion,
            enableStorageOptimizer,
            ignoreFileNames
        )
    }
}