package com.larionov.chatstats.td.auth

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("td/auth")
class TdAuthorizationController(private val authorizationState: AuthorizationState) {
    @PostMapping("code")
    fun enterCode(@RequestParam code: String) {
        authorizationState.code.set(code)
    }

    @PostMapping("password")
    fun enterPassword(@RequestParam password: String) {
        authorizationState.password.set(password)
    }
}