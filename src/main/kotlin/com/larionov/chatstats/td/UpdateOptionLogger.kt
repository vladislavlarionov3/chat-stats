package com.larionov.chatstats.td

import org.drinkless.tdlib.TdApi
import org.springframework.stereotype.Component

@Component
class UpdateOptionLogger {
    fun log(result: TdApi.UpdateOption) {
        println("Option ${result.name} updated")
    }
}