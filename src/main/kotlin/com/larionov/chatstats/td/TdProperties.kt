package com.larionov.chatstats.td

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("td")
data class TdProperties(
    val useTestDc: Boolean = true,
    val databaseDirectory: String = "td-database",
    val filesDirectory: String = "",
    val databaseEncryptionKey: String = "",
    val useFileDatabase: Boolean = true,
    val useChatInfoDatabase: Boolean = true,
    val useMessageDatabase: Boolean = true,
    val phone: String = "",
    val useSecretChats: Boolean = true,
    val apiId: Int = 0,
    val apiHash: String = "",
    val systemLanguageCode: String = "",
    val deviceModel: String = "",
    val systemVersion: String = "",
    val applicationVersion: String = "",
    val enableStorageOptimizer: Boolean = false,
    val ignoreFileNames: Boolean = false,
    val verbosityLevel: Int = 1
)