package com.larionov.chatstats.td

import org.drinkless.tdlib.Client
import org.springframework.stereotype.Component

@Component
class TdExceptionHandler : Client.ExceptionHandler {
    override fun onException(throwable: Throwable) {
        throwable.printStackTrace()
    }
}