package com.larionov.chatstats.td

import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import org.drinkless.tdlib.Client
import org.drinkless.tdlib.TdApi
import org.springframework.retry.support.RetryTemplate
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicReference
import kotlin.time.DurationUnit
import kotlin.time.toDuration

@Component
class ClientWrapper(
    val properties: TdProperties,
    resultHandler: DelegatingTdResultHandler,
    exceptionHandler: TdExceptionHandler
) {
    val client = Client.create(resultHandler, exceptionHandler, exceptionHandler)!!

    @PostConstruct
    fun init() {
        Client.execute(TdApi.SetLogVerbosityLevel(properties.verbosityLevel))
        Client.setLogMessageHandler(properties.verbosityLevel) { _: Int, message: String? ->
            println(message)
        }
    }

    @PreDestroy
    fun close() {
        sendSync(TdApi.Close())
        println("Td client closed")
    }

    fun send(function: TdApi.Function<out TdApi.Object>, resultHandler: Client.ResultHandler) =
        client.send(function, resultHandler)

    fun sendSync(apiFunction: TdApi.Function<out TdApi.Object>): TdApi.Object =
        AtomicReference<TdApi.Object>().run {
            client.send(apiFunction, ::set)
            return RetryTemplate.builder()
                .fixedBackoff(10.toDuration(DurationUnit.SECONDS).inWholeMilliseconds)
                .withTimeout(60.toDuration(DurationUnit.SECONDS).inWholeMilliseconds)
                .build()
                .execute<TdApi.Object, RuntimeException> { get()!! }
        }

    companion object {
        init {
            System.loadLibrary("tdjni")
        }
    }
}

