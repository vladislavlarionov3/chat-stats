package com.larionov.chatstats.td

import com.larionov.chatstats.td.auth.UpdateAuthorizationStateHandler
import org.drinkless.tdlib.Client
import org.drinkless.tdlib.TdApi
import org.springframework.stereotype.Component

@Component
class DelegatingTdResultHandler(
    private val updateAuthorizationStateHandler: UpdateAuthorizationStateHandler,
    private val updateOptionLogger: UpdateOptionLogger
) : Client.ResultHandler {
    override fun onResult(result: TdApi.Object) {
        when (result) {
            is TdApi.UpdateAuthorizationState -> updateAuthorizationStateHandler.handle(result)
            is TdApi.UpdateOption -> updateOptionLogger.log(result)
            is TdApi.UpdateUser -> println("UpdateUser ${result.user.firstName} ${result.user.lastName} ")
            is TdApi.UpdateNewChat -> println("UpdateNewChat ${result.chat.title}")
            is TdApi.UpdateChatLastMessage -> println("UpdateChatLastMessage  ${result.lastMessage.chatId} ${result.lastMessage.authorSignature}")

            else -> println("Unhandled TdApi.Object: ${result::class.simpleName}")
        }
    }


}