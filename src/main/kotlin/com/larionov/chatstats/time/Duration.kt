package com.larionov.chatstats.time

import java.time.Duration

operator fun Duration.div(value: Long) = dividedBy(value)!!

operator fun Duration.div(value: Int) = div(value.toLong())

operator fun Duration.div(value: Double) = div(value.toLong())

operator fun Duration.div(value: Duration) = toMillis().toDouble() / value.toMillis()

fun Long.milliseconds() = Duration.ofMillis(this)!!

fun Double.milliseconds() = toLong().milliseconds()

fun Int.minutes() = Duration.ofMinutes(this.toLong())!!
