package com.larionov.chatstats.stats.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.larionov.chatstats.chat.Chat
import com.larionov.chatstats.time.div
import com.larionov.chatstats.time.milliseconds
import java.time.Duration
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import kotlin.math.roundToLong

const val HOURS_IN_WORKING_DAY = 8

@Suppress("unused", "MemberVisibilityCanBePrivate")
class TotalStats(
    chats: List<Chat>,
    minSessionDuration: Duration,
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val periodFrom: LocalDate,
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val periodTo: LocalDate,
    val countOfPeriodDays: Long = periodFrom.until(periodTo, ChronoUnit.DAYS),
    val countOfWorkingDays: Long = ((5.0 / 7) * countOfPeriodDays.toDouble()).roundToLong()
) {
    val chatStats = chats
        .filter { it.messages().isNotEmpty() }
        .map { ChatStats(it, minSessionDuration, periodFrom, periodTo, countOfWorkingDays) }
        .sortedByDescending { it.totalSessionsDuration }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val totalSessionsDuration: Duration = chatStats
        .sumOf { it.totalSessionsDuration.toMillis() }
        .milliseconds()

    val totalSessionsDurationInWorkingDays = totalSessionsDuration.toHours().toDouble() / HOURS_IN_WORKING_DAY

    val percentageOfWorkingTime =
        100 * (totalSessionsDuration / Duration.ofHours(HOURS_IN_WORKING_DAY * countOfWorkingDays))
}