package com.larionov.chatstats.stats.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.larionov.chatstats.chat.Chat
import com.larionov.chatstats.session.sessions
import com.larionov.chatstats.time.div
import com.larionov.chatstats.time.milliseconds
import java.time.Duration
import java.time.LocalDate

@Suppress("unused", "MemberVisibilityCanBePrivate")
class ChatStats(
    chat: Chat,
    minSessionDuration: Duration,
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val periodFrom: LocalDate,
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val periodTo: LocalDate,
    countOfWorkingDays: Long,
) {
    private val sessions = chat.sessions(minSessionDuration)

    private val dates = chat.dates()

    val chatName = chat.name

    val countOfMessages = chat.messages().count()

    val countOfSessions = sessions.count()

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val avgSessionDuration = sessions.map { it.duration().toMillis() }
        .average()
        .milliseconds()

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val totalSessionsDuration = sessions.sumOf { it.duration().toMillis() }.milliseconds()

    val countOfDistinctDays = dates.count()

    val avgMessagesPerWorkingDay = countOfMessages.toDouble() / countOfWorkingDays

    val avgMessagesPerDistinctDay = countOfMessages.toDouble() / countOfDistinctDays

    val avgMessagesPesSession = countOfMessages.toDouble() / countOfSessions

    val avgSessionsPerWorkingDay = countOfSessions.toDouble() / countOfWorkingDays

    val avgSessionsPerDistinctDay = countOfSessions.toDouble() / countOfDistinctDays

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val avgSessionsDurationPerWorkingDay = totalSessionsDuration / countOfWorkingDays

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val avgSessionsDurationPerDistinctDay = totalSessionsDuration / countOfDistinctDays

    val percentageOfWorkingTime =
        100 * (totalSessionsDuration / Duration.ofHours(HOURS_IN_WORKING_DAY * countOfWorkingDays))
}