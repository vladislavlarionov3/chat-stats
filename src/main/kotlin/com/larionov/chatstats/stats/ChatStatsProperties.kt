package com.larionov.chatstats.stats

import com.larionov.chatstats.time.minutes
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import java.time.LocalDate

@Configuration
@ConfigurationProperties("chat-stats")
data class ChatStatsProperties(
    val chatIds: List<Long> = listOf(
        613721114,//Yaroslav.Mih
        484338590,//Katerina
        848875650,//Борис
        70596024,//Daniil
        124987693,//Ilya
        165217194,//Ivan
        480808403,//Maria
        4290587,//Vitaliy
        481864720,//Yaroslav.Mim
        560408867,//Даша
    ),
    val toDateTime: LocalDate = LocalDate.now(),
    val fromDateTime: LocalDate = toDateTime.minusDays(14),
    val minSessionDurationInMinutes: Int = 15
) {
    fun minSessionDuration() = minSessionDurationInMinutes.minutes()
}