package com.larionov.chatstats.stats

import com.larionov.chatstats.chat.provider.ChatProvider
import com.larionov.chatstats.stats.domain.TotalStats
import org.springframework.stereotype.Component

@Component
class ChatStatsProvider(
    private val chatProvider: ChatProvider,
    private val properties: ChatStatsProperties
) {
    fun get() = properties.run {
        TotalStats(
            chatProvider.getAll(chatIds, fromDateTime),
            minSessionDuration(),
            fromDateTime,
            toDateTime
        )
    }


}