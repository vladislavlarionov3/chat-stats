package com.larionov.chatstats.stats

import com.larionov.chatstats.logging.JsonConsoleLogger
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
@Profile("command-line-runner")
class ChatStatsCommandLineRunner(
    private val statsProvider: ChatStatsProvider,
    private val logger: JsonConsoleLogger,
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        TimeUnit.SECONDS.sleep(30)
        logger.log(statsProvider.get())
    }

}