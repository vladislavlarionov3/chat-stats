package com.larionov.chatstats.stats

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("chat-stats")
class ChatStatsController(private val statsProvider: ChatStatsProvider) {

    @GetMapping
    fun get() = statsProvider.get()

}