package com.larionov.chatstats.logging

import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import org.springframework.stereotype.Component
import java.time.LocalDate
import kotlin.time.Duration
import kotlin.time.toJavaDuration

@Component
class JsonConsoleLogger {
    private val gson = GsonBuilder()
        .registerTypeAdapter(
            Duration::class.java,
            JsonSerializer { it: Duration, _, _ ->
                JsonPrimitive(it.toJavaDuration().toString())
            }
        )
        .registerTypeAdapter(
            LocalDate::class.java,
            JsonSerializer { it: LocalDate, _, _ ->
                JsonPrimitive(it.toString())
            }
        )
        .setPrettyPrinting()
        .create()

    fun log(any: Any) = println(gson.toJson(any)!!)
}

