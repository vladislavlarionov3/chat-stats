package com.larionov.chatstats.session

import com.larionov.chatstats.chat.Chat
import java.time.Duration

class Session(private val firstMessage: Chat.ChatMessage, private val minDuration: Duration) {
    fun duration() = Duration.between(
        firstMessage.message.dateTime.minus(minDuration),
        firstMessage.lastMessage().message.dateTime
    )!!

    private fun Chat.ChatMessage.lastMessage(): Chat.ChatMessage =
        if (isLast() || next().isSessionStart(minDuration)) this
        else next().lastMessage()
}

fun Chat.ChatMessage.isSessionStart(minSessionDuration: Duration) =
    isFirst() || betweenPreviousDuration() > minSessionDuration

fun Chat.sessions(minSessionDuration: Duration) = messages()
    .filter { it.isSessionStart(minSessionDuration) }
    .map { Session(it, minSessionDuration) }