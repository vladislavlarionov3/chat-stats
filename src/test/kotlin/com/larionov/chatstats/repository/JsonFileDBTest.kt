package com.larionov.chatstats.repository

import com.google.gson.reflect.TypeToken
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

internal class JsonFileDBTest {

    lateinit var repository: JsonFileRepository<Entity>

    private val filePath = JsonFileDBTest::class.simpleName + "-db.json"

    @BeforeEach
    internal fun setUp() {
        repository = JsonFileRepository(filePath, object : TypeToken<List<Entity>>() {}.type)
    }

    @AfterEach
    internal fun tearDown() {
        File(filePath).delete()
    }

    @Test
    fun save() {
        repository.save(listOf(Entity("field")))
    }

    @Test
    fun findAll() {
        val entity = Entity("field")

        repository.save(listOf(entity))
        val allSaved: List<Entity> = repository.findAll()

        assertEquals(1, allSaved.size)
        assertEquals(entity.field, allSaved.first().field)
    }

    class Entity(val field: String)

}